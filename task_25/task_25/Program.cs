﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_25
{
    class Program
    {
        static void Main(string[] args)
        {
            bool isnumber = false;
            string input_a;
            int input_b;

            do
            {
                Console.Clear();

                Console.WriteLine("Type in an Integer (whole number):");
                input_a = Console.ReadLine();
                int.TryParse(input_a, out input_b);

                if (input_b == 0)
                {
                    Console.WriteLine("\n>    Invalid number! Try typing in an Integer (whole number) that is not equal to 0.");

                    Console.Clear();
                }
                else
                {
                    isnumber = true;
                }
            } while (isnumber == false);

            Console.Clear();

            Console.WriteLine($"Your number is {input_b}.");
        }
    }
}
