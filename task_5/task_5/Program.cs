﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_5
{
    class Program
    {
        static void Main(string[] args)
        {
            string time_s;
            int time_i;
            bool correct_format = false;

            do
            {
                Console.WriteLine("Please enter 24-hour time you want to convert to 12-hour time");
                time_s = Console.ReadLine();
                int.TryParse(time_s, out time_i);

                if (time_i >= 13 && time_i <= 23)
                {
                    time_i -= 12;

                    Console.WriteLine($"The time is {time_i} p.m.");
                    correct_format = true;
                }
                else if (time_i >= 1 && time_i <= 11)
                {
                    Console.WriteLine($"The time is {time_i} a.m.");
                    correct_format = true;
                }

                if (time_i == 24)
                {
                    Console.WriteLine("The time is 12 a.m.");
                    correct_format = true;
                }

                else if (time_i == 12)
                {
                    Console.WriteLine("The time is 12 p.m.");
                    correct_format = true;
                }
            } while (correct_format == false);
        }
    }
}
