﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task01
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please Enter Your Name:");
            var name = Console.ReadLine();

            Console.WriteLine("Please Enter Your Age:");
            var age = Console.ReadLine();

            Console.Clear();

            Console.WriteLine($"Name: {name}");
            Console.WriteLine($"Age: {age}");

            Console.WriteLine("3 Different Way to Print a String");

            Console.WriteLine("String Concatation");
            Console.WriteLine("Console.WriteLine(\"Your Name is + name + and Your Age is + age +\");");

            Console.WriteLine("Positional Formatting");
            Console.WriteLine("Console.WriteLine(\"Your Name is {0} and Your Age is {1}\", name, age);");

            Console.WriteLine("String Interpolation");
            Console.WriteLine("Console.WriteLine($\"Your Name is {name} and Your Age is {age}\");");
        }
    }
}
