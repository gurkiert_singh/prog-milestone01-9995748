﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_18
{
    class Program
    {
        static void Main(string[] args)
        {
            var names = new List<Tuple<string, string, int>>();
            names.Add(Tuple.Create("Fred", "August", 18));
            names.Add(Tuple.Create("Sam", "October", 11));
            names.Add(Tuple.Create("Alisha", "December", 24));

            foreach (var a in names)
            {
                Console.WriteLine($"My name is {a.Item1} and I was born on the {a.Item3} {a.Item2}");
            }
        }
    }
}
