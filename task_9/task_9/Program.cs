﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_9
{
    class Program
    {
        static void Main(string[] args)
        {
            int year;
            var a = 2016;
            var counter = 2036;

            Console.WriteLine("Enter the year you want to check");

            year = int.Parse(Console.ReadLine());

            for (a = 2016; a <= counter; a++) ;

            if (year % 4 == 0)
            {
                Console.WriteLine($"Year {year} is a leap year");
            }
            else{
                Console.WriteLine($"Year {year} is not a leap year");
            }
        }
    }
}
