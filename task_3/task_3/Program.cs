﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(" If you want km to mile press \"k\"\n It you wasnt miles to km press \"m\"");

            var distance = Console.ReadLine();
            const double mile2km = 1.609344;
            const double km2mile = 0.621371;
            Console.Clear();
            switch (distance)
            {
                case "k":
                    Console.WriteLine("Enter Distance in KM that you want to convert to Miles:");
                    var KM = double.Parse(Console.ReadLine());
                    var calmile = System.Math.Round(KM * km2mile, 2);
                    Console.WriteLine($"{KM} KM is {calmile} Miles");
                    break;
                case "m":
                    Console.WriteLine("Enter Distance in Miles that you want to convert to KM:");
                    var mile = double.Parse(Console.ReadLine());
                    var calkm = System.Math.Round(mile * mile2km, 2);
                    Console.WriteLine($"{mile} MIles is {calkm} KM");
                    break;

            }
        }
    }
}
