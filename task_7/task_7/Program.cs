﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_7
{
    class Program
    {
        static void Main(string[] args)
        {
            int n, i;
            Console.WriteLine("Please enter in a number:");
            n = int.Parse(Console.ReadLine());
            
            for (i = 1; i <=12; i++)
            {
                Console.WriteLine("{0}*{1}={2}", n, i, (n * i));
            }
        }

    }
}
